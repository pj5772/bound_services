package com.example.boundservicesassignment.boundservicesassignment;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.TextView;

import java.util.Random;
import java.lang.Math;

public class RandomGeneratorService extends Service {
    // Binder given to clients
    private final IBinder mBinder = new LocalBinder();
    // Random number generator
    private final Random mGenerator = new Random();

    public final String TAG = "com.example.boundserv";

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        RandomGeneratorService getService() {
            // Return this instance of LocalService so clients can call public methods
            return RandomGeneratorService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    // Recursive function to
    // return gcd of a and b
    // based on Euclidean algorithm
    private int gcd(int a, int b)
    {
        // Everything divides 0
        if (a == 0 || b == 0)
            return 0;

        // base case
        if (a == b)
            return a;

        // a is greater
        if (a > b)
            return gcd(a-b, b);

        return gcd(a, b-a);
    }

    private boolean coprime(int a, int b) {
        return (gcd(b,a) == 1);
    }

    //approximates pi using random numbers
    public double getPiApprox(int n_trials, int upper_bound) {
        double total_coprime = 0;

        for (int i = 0; i < n_trials; i++) {
            //if two random numbers are coprime increase the total number of coprimes
            if (coprime(mGenerator.nextInt(upper_bound),mGenerator.nextInt(upper_bound)))
                total_coprime++;
        }

        //percentage of trails that ended coprime
        double percent_coprime = total_coprime / ((double) n_trials);

        //aprroximation of pi
        //
        //  probability_coprime = 6 / (pi^2)
        //
        double pi_approx = Math.sqrt(6 / percent_coprime);

        return pi_approx;
    }
}
