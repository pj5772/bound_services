package com.example.boundservicesassignment.boundservicesassignment;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimeService extends Service {
    /** Command to the service to display a message */
    static final int GET_TIME = 1;

    /**
     * Handler incoming messages
     */
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                //if message is GET_TIME
                case GET_TIME:
                    //get current time
                    Date currentTime = Calendar.getInstance().getTime();

                    //convert it to string
                    DateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
                    String strDate = dateFormat.format();

                    //show a toast saying the time
                    Toast.makeText(getApplicationContext(), Date.toString(), Toast.LENGTH_SHORT).show();
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    /**
     * Target we publish for clients to send messages to IncomingHandler.
     */
    final Messenger mMessenger = new Messenger(new IncomingHandler());

    /**
     * When binding to the service, we return an interface to our messenger
     * for sending messages to the service.
     */
    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }
}