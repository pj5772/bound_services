package com.example.boundservicesassignment.boundservicesassignment;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //random pi approximator service
    RandomGeneratorService randBoundService;
    //holds whether random pi approximator is bound
    boolean randServiceBound = false;

    //holds messenger for time service
    Messenger timeService = null;
    //holds whether the timeService is bound
    boolean timeServiceBound = false;

    public final String TAG = "com.example.boundserv";

    public void onRandomButtonClick(View v) {
        //if random pi approximator service is bound
        if (randServiceBound) {
            //get approximation of pi
            double pi_approx = randBoundService.getPiApprox(1000, 1000);

            //set text of textview
            TextView randTextView = (TextView) findViewById(R.id.random_pi_textview);
            randTextView.setText(String.valueOf(pi_approx));
        }
    }

    public void getTime(View v) {
        //if we are bound to time service
        if (timeServiceBound) {
            // Create and send a message to the service, using a supported 'what' value
            Message msg = Message.obtain(null, TimeService.GET_TIME);
            try {
                timeService.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Bind to time service
        bindService(new Intent(this, TimeService.class), mConnection,
                Context.BIND_AUTO_CREATE);

        //start random pi aproximator service
        Intent intent = new Intent(this, RandomGeneratorService.class);
        startService(intent);
        bindService(intent, randServiceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();

        // Unbind from time service
        if (timeServiceBound) {
            unbindService(mConnection);
            timeServiceBound = false;
        }

        //application is exiting so unbind pi aproximator service
        if (randServiceBound) {
            unbindService(randServiceConnection);
            randServiceBound = false;
        }
    }

    //create service connection for generating pi with random numbers
    private ServiceConnection randServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            randServiceBound = false;
        }
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            RandomGeneratorService.LocalBinder myBinder = (RandomGeneratorService.LocalBinder) service;
            randBoundService = myBinder.getService();
            randServiceBound = true;
        }
    };

    //connection for interacting with time service
    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the object we can use to
            // interact with the service.  We are communicating with the
            // service using a Messenger, so here we get a client-side
            // representation of that from the raw IBinder object.
            timeService = new Messenger(service);
            timeServiceBound = true;
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected
            timeService = null;
            timeServiceBound = false;
        }
    };
}
